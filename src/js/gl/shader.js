"use strict";

/**
 * Represents a compiled OpenGL ES shader program.
 * 
 * Uniform locations are cached for future use.
 * Uniform upload functions are not mapped over, because it's really tedious and
 * honestly it isn't worth doing because I'd just be reimplementing the normal
 * function signatures.
 */
class Shader {
    /**
     * Create a shader program from vertex shader and fragment shader sources.
     * @param {String} vs Vertex shader source.
     * @param {String} fs Fragment shader source.
     * @param {Array<String>?} uniforms If specified, will lookup uniform locations ahead of time.
     */
    constructor(vs, fs, uniforms = null) {
        this._uniformCache = {};
        
        const vsh = gl.createShader(gl.VERTEX_SHADER);
        gl.shaderSource(vsh, vs);
        gl.compileShader(vsh);
        let emsg = gl.getShaderInfoLog(vsh);
        if (emsg.length > 0) {
            throw new Error(`Error compiling vertex shader:\n\n${emsg}`);
        }
        
        const fsh = gl.createShader(gl.FRAGMENT_SHADER);
        gl.shaderSource(fsh, fs);
        gl.compileShader(fsh);
        emsg = gl.getShaderInfoLog(fsh);
        if (emsg.length > 0) {
            throw new Error(`Error compiling fragment shader:\n\n${emsg}`);
        }
        
        this.prog = gl.createProgram();
        gl.attachShader(this.prog, vsh);
        gl.attachShader(this.prog, fsh);
        gl.linkProgram(this.prog);
        emsg = gl.getProgramInfoLog(this.prog);
        if (emsg.length > 0) {
            throw new Error(`Error linking shader program:\n\n${emsg}`);
        }
        
        gl.deleteShader(vsh);
        gl.deleteShader(fsh);
        
        if (uniforms && Array.isArray(uniforms)) {
            for (const un of uniforms) {
                this.lookup(un);
            }
        }
    }
    
    unload() {
        gl.deleteProgram(this.prog);
    }
    
    /**
     * Activates this shader program.
     */
    use() {
        gl.useProgram(this.prog);
    }
    
    /**
     * Get and cache a uniform location.
     */
    lookup(name) {
        if (!this._uniformCache.hasOwnProperty(name)) {
            this._uniformCache[name] = gl.getUniformLocation(this.prog, name);
        }
        
        return this._uniformCache[name];
    }
}
