"use strict";

/**
 * Represents an OpenGL ES texture.
 */
class Texture {
    /**
     * Create a new texture.
     * @param {Image} img The image to use for the texture.
     * @param {Object} config Additional texture parameters.
     *                        Set wrap to affect (S, T) out-of-bounds sampling,
     *                        or wraps and wrapt for individual control.
     *                        Set filter to affect texel filtering, or filtermin
     *                        and filtermag for different filtering when scaling
     *                        up or down.
     *                        The defaults are GL_CLAMP_TO_EDGE and GL_NEAREST.
     */
    constructor(img, config = {}) {
        if (!config || typeof(config) != "object") { config = {}; }
        
        this.texture = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_2D, this.texture);
        
        if (config.hasOwnProperty("wrap")) {
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, config.wrap);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, config.wrap);
        }
        else if (config.hasOwnProperty("wraps") && config.hasOwnProperty("wrapt")) {
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, config.wraps);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, config.wrapt);
        }
        else {
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        }
        
        if (config.hasOwnProperty("filter")) {
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, config.filter);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, config.filter);
        }
        else if (config.hasOwnProperty("filtermin") & config.hasOwnProperty("filtermag")) {
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, config.filtermin);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, config.filtermag);
        }
        else {
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        }
        
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, img.width, img.height, 0, gl.RGBA, gl.UNSIGNED_BYTE, img);
        gl.generateMipmap(gl.TEXTURE_2D);
    }
    
    unload() {
        gl.deleteTexture(this.texture);
    }
    
    /**
     * Bind this texture to a texture unit. GL_TEXTURE0 through GL_TEXTURE15 are supported.
     * @param {Number} [unit=0] Which texture unit to bind to. GL_TEXTURE0 by default.
     */
    bind(unit = gl.TEXTURE0) {
        gl.activeTexture(unit);
        gl.bindTexture(gl.TEXTURE_2D, this.texture);
    }
}
