"use strict";

class WebGL {
    constructor() {
        this.canvas = document.createElement("canvas");
        document.getElementById("view").appendChild(this.canvas);
        this.canvas.width = this.canvas.clientWidth;
        this.canvas.height = this.canvas.clientHeight;
        
        this.robv = new ResizeObserver(() => this._resize());
        this.robv.observe(this.canvas.parentNode, {box: "content-box"});
        
        this.resizehooks = [];
        
        this.gl = this.canvas.getContext("webgl2");
        this.gl.viewport(0, 0, this.canvas.width, this.canvas.height);
        this.gl.pixelStorei(this.gl.UNPACK_FLIP_Y_WEBGL, true);
    }
    
    /**
     * Add a callback to call when resizes occur.
     * @param {Function} f Callback
     */
    resizeHook(f) {
        this.resizehooks.push(f);
    }
    
    /**
     * Resizes the canvas.
     */
    _resize() {
        const w = this.canvas.clientWidth;
        const h = this.canvas.clientHeight;
        
        if (this.canvas.width != w || this.canvas.height != h) {
            this.canvas.width = w;
            this.canvas.height = h;
            this.gl.viewport(0, 0, w, h);
            
            for (const f of this.resizehooks) {
                f(w, h);
            }
        }
    }
    
    /**
     * Checks for OpenGL errors.
     */
    errorCheck(context = "") {
        let e = this.gl.getError()
        
        if (e == 0) {
            console.log(context, "No error.");
            return;
        }
        
        while (e != 0) {
            switch (e) {
                case this.gl.INVALID_ENUM: { console.log(context, "Invalid enumeration."); } break;
                case this.gl.INVALID_VALUE: { console.log(context, "Invalid value."); } break;
                case this.gl.INVALID_OPERATION: { console.log(context, "Invalid operation."); } break;
                case this.gl.INVALID_FRAMEBUFFER_OPERATION: { console.log(context, "Invalid framebuffer operation."); } break;
                case this.gl.OUT_OF_MEMORY: { console.log(context, "Out of memory."); } break;
                case this.gl.CONTEXT_LOST_WEBGL: { console.log(context, "WebGL2 context lost."); } break;
                default: { console.log(context, `Unknown error code ${e}.`); } break;
            }
            
            e = this.gl.getError();
        }
    }
}

const webgl = new WebGL();
const glm = glMatrix;
const gl = webgl.gl;
