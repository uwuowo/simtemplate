'use strict';

class Poolable {
    constructor() {
        this._next = null;
    }
    
    get next() {
        return this._next;
    }
    
    set next(n) {
        this._next = n;
    }
}
