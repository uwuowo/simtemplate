'use strict';

/**
 * Defines the type of object pool
 * @readonly
 * @enum {Number}
 */
const POOL_TYPE = {
    /** Statically sized pool. */
    STATIC: 0,
    /** Dynamically sized pool. */
    DYNAMIC: 1
};

/**
 * An object pool using free lists. Can be statically or dynamically sized,
 * with a configurable growth factor if dynamically sized.
 */
class Pool {
    /**
     * @param {Number} type One of POOL_TYPE.
     * @param {Poolable} itemtype Type of object to store. Must be a subclass of Poolable.
     *                            The constructor of this type should be empty, or at least not take any required arguments.
     *                            You should define a separate init function for when you pull from the pool.
     * @param {Number} size Initial pool size. Integers only.
     * @param {Number} [growth=size + 10] When an integer, growth is a fixed size.
     *                                    When a real, the new size is a product of the current size and this value.
     *                                    This is size + 10 by default.
     */
    constructor(type, itemtype, size, growth = size + 10) {
        // The pool type must be one of the POOL_TYPE constants.
        if (!Object.values(POOL_TYPE).includes(type)) {
            throw new Error("Pool type must be one of POOL_TYPE.");
        }
        
        // The pool size must be at least 1 and an integer.
        if (size < 1 || !Number.isInteger(size)) {
            throw new Error("Pool size must be >= 1 and an integer.");
        }
        
        this._type = type;
        this._itemtype = itemtype;
        this._size = Math.floor(size);
        this._growth = growth;
        
        // We require metadata for the free list. There are a few ways this
        // could have been implemented, I preferred your item types being a
        // derivative of a type containing said metadata. The next alternative
        // would have been setting the properties we expect instead, but that
        // feels messy.
        if (!(this._itemtype.prototype instanceof Poolable)) {
            throw new Error("Pool only accepts Poolable subclasses.");
        }
        
        this._items = new Array(this._size);
        for (let i = 0, l = this._size; i < l; i++) {
            this._items[i] = new this._itemtype;
        }
        
        this._nextAvailable = this._items[0];
        
        for (let i = 0, l = this._size - 1; i < l; i++) {
            this._items[i]._next = this._items[i + 1];
        }
        
        switch (this._type) {
            case POOL_TYPE.STATIC: {
                this.pull = this._pullStatic;
            }
            break;
            
            case POOL_TYPE.DYNAMIC: {
                this.pull = this._pullDynamic;
                
                if (Number.isInteger(this._growth)) {
                    this._growSize = this._growStatic;
                }
                else {
                    this._growSize = this._growExpo;
                }
            }
            break;
        }
    }
    
    /**
     * Get a single item from the pool. If the pool is STATIC, this can return null if the pool is empty.
     * Note that this is only here for documentation. Implementation is set during construction based on
     * pool type.
     * @returns {ItemType?} next item
     */
    pull() {}
    
    /**
     * Calls pull as many times as you request and returns the results in an array.
     * @param {Number} count How many items to pull
     * @returns {Array<ItemType?>} next items
     */
    pullMany(count) {
        count = Math.floor(count);
        const ret = new Array(count);
        for (let i = 0; i < count; i++) {
            ret[i] = this.pull();
        }
        
        return ret;
    }
    
    /**
     * Get a single item from the pool. This can return null if the pool is empty.
     * @returns {ItemType?} next item
     */
    _pullStatic() {
        if (!this._nextAvailable) {
            return null;
        }

        const ret = this._nextAvailable;
        this._nextAvailable = ret.next;
        return ret;
    }
    
    /**
     * Get a single item from the pool.
     * @returns {ItemType} next item
     */
    _pullDynamic() {
        if (!this._nextAvailable) {
            this._grow();
        }

        const ret = this._nextAvailable;
        this._nextAvailable = ret.next;
        return ret;
    }
    
    /**
     * Grows the items array according to the growth factor. Only used with POOL_TYPE.DYNAMIC.
     */
    _grow() {
        this._growItems(this._growSize());
    }
    
    /**
     * Increases the tracked size of the items array. Only used with POOL_TYPE.DYNAMIC.
     * Can be static or exponential, implementation is set during construction.
     * @returns {Number} old size
     */
    _growSize() {}
    
    /**
     * Increases the tracked size of the items array by a static amount.
     * @returns {Number} old size
     */
    _growStatic() {
        const old = this._size;
        this._size += this._growth;
        return old;
    }
    
    /**
     * Increases the tracked size of the items array exponentially.
     * @returns {Number} old size
     */
    _growExpo() {
        const old = this._size;
        this._size += Math.floor(this._size * this._growth);
        return old;
    }
    
    /**
     * Recreates the items array with the new size, copying over the contents of the old and allocating to fill the rest.
     */
    _growItems(oldsize) {
        const newitems = new Array(this._size);
        
        for (let i = 0; i < oldsize; i++) {
            newitems[i] = this._items[i];
        }
        
        for (let i = oldsize; i < this._size; i++) {
            newitems[i] = new this._itemtype;
        }
        
        this._nextAvailable = newitems[oldsize];
        
        for (let i = oldsize; i < this._size - 1; i++) {
            newitems[i].next = newitems[i + 1];
        }
        
        newitems[this._size - 1].next = null;
        
        this._items = newitems;
    }
    
    /**
     * Return an item to the pool.
     * @param {ItemType} item The item being returned
     * @returns this
     */
    push(item) {
        item.next = this._nextAvailable;
        this._nextAvailable = item;
        return this;
    }
    
    /**
     * Return many items to the pool. Calls push for each item.
     * @param {Array<ItemType>} items The item being returned
     * @returns this
     */
    pushMany(items) {
        for (let i = 0, l = items.length; i < l; i++) {
            this.push(items[i]);
        }
        
        return this;
    }
    
    /**
     * Get the size of the pool.
     * @returns {Number}
     */
    get size() {
        return this._size;
    }
    
    /**
     * Get the growth factor of the pool.
     * @returns {Number}
     */
    get growth() {
        return this._growth;
    }
    
    /**
     * Get the pool item type.
     * @returns {ItemType}
     */
    get itemtype() {
        return this._itemtype;
    }
    
    /**
     * Get the type of pool.
     * @returns {Number] One of POOL_TYPE.STATIC or POOL_TYPE.DYNAMIC
     */
    get type() {
        return this._type;
    }
}
