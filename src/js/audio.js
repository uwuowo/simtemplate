"use strict";

/**
 * Contains the audio context and global audio controls.
 */
class AudioControl {
    constructor() {
        this.context = new AudioContext();
        this.drc = this.context.createDynamicsCompressor();
        this.drc.connect(this.context.destination);
    }
}

/**
 * Represents an Audio resource.
 * 
 * Has a gain node attached for volume control.
 */
class AudioSource {
    /**
     * @param {ArrayBuffer} data The data from file containing the audio data.
     */
    constructor(data) {
        this._realconstructor(data);
    }
    
    async _realconstructor(data) {
        await ac.context.decodeAudioData(data,
            (r) => { this._buffer = r; },
            (e) => { throw new Error(`Error decoding audio: ${e}`); });
    }
    
    unload() {}
    
    /**
     * Play the audio source. This creates a new buffer source every call,
     * pointing to the same shared AudioBuffer. A new gain node is created
     * as well. Both will be destroyed once their references are gone.
     * @param {Number} vol=1.0 Volume. Be very careful with this value.
     */
    play(vol = 1.0) {
        const node = ac.context.createBufferSource();
        const gain = ac.context.createGain();
        node.buffer = this._buffer;
        gain.gain.setValueAtTime(vol, ac.context.currentTime);
        node.connect(gain).connect(ac.drc);
        
        node.onended = (e) => {
            gain.disconnect();
            node.disconnect();
        };
        
        node.start();
    }
}

/**
 * A special version of AudioSource specialized for looped sound.
 */
class LoopedAudioSource extends AudioSource {
    constructor(data) {
        super(data);
        
        this.node = null;
        this.gain = null;
    }
    
    /**
     * Useful for background music or something that just needs to be played
     * over and over. This only works if not currently looping.
     * @param {Number} vol=1.0 Volume. Be very careful with this value.
     * @param {Number} max=2.0 Max volume. For your safety this is 2.0 by default.
     *                         Change at your own risk.
     */
    loop(vol = 1.0, max = 2.0) {
        if (!this.node) {
            this._max = max;
            this.node = ac.context.createBufferSource();
            this.gain = ac.context.createGain();
            this.node.buffer = this._buffer;
            this.gain.gain.setValueAtTime(Math.min(vol, this._max), ac.context.currentTime);
            this.node.connect(this.gain).connect(ac.drc);
            
            this.node.loop = true;
            this.node.start();
        }
    }
    
    /**
     * Set the gain of the looped sound. 1.0 is unchanged.
     * Lower is more quiet, higher is more loud. Values above
     * the set maximum when starting the loop won't have an effect.
     * @param {Number} vol Volume. Be very careful with this value.
     */
    setVolume(vol) {
        if (this.node) {
            this.gain.gain.setValueAtTime(Math.min(vol, this._max), ac.context.currentTime);
        }
    }
    
    /**
     * Adjust the gain of the looped sound by a delta.
     * This method could be pretty dangerous, make sure you understand
     * what it does and how you're calling it. Values above
     * the set maximum when starting the loop won't have an effect.
     * @param {Number} delta Volume delta. Be very VERY careful with this value.
     */
    adjustVolume(delta) {
        if (this.node) {
            const nv = this.gain.gain.value + delta;
            this.gain.gain.setValueAtTime(clamp(nv, 0.0, this._max), ac.context.currentTime);
        }
    }
    
    /**
     * Stops the looped sound.
     */
    stop() {
        if (this.node) {
            this.node.stop();
            this.gain.disconnect();
            this.node.disconnect();
            this.gain = null;
            this.node = null;
        }
    }
}

const ac = new AudioControl();
