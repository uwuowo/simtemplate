'use strict';

/**
 * This can be used as a synchronization point for many async operations.
 * Your async code should call the test function returned, which calls your
 * predicate.
 * @param {Function} predicate A function that should evaluate to either true or false.
 */
const AsyncSync = (predicate) => {
    const resolve = {};
    resolve.sync = new Promise(res => {
        resolve.test = () => {
            if (predicate()) {
                res();
            }
        };
    });
    
    return resolve;
};

/**
 * This is a convenience wrapper over AsyncSync. It sets up a counter for you
 * so you don't need to do it yourself. If the supplied predicate is null or
 * not a function it will be set to a function that just returns true.
 * @param {Number} limit Counter limit.
 * @param {Function?} predicate Optional function that should evaluate to either true or false.
 */
const CounterSync = (limit, predicate = null) => {
    let counter = 0;
    if (!predicate || typeof(predicate) != "function") {
        predicate = () => true;
    }
    
    return AsyncSync(() => ++counter >= limit && predicate());
};

/**
 * You can use await on this to pass a given amount of time. Uses setTimeout
 * so technically the time may be way off, or theoretically never lapse.
 * @param {Number} time Time in milliseconds.
 */
const sleep = (time) => {
    return new Promise(r => {
        setTimeout(() => { r(); }, time);
    });
};

/**
 * A clamp function, because this somehow still isn't standard.
 */
const clamp = (x, min, max) => {
    return Math.max(min, Math.min(x, max));
};
