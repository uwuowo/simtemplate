'use strict';

/**
 * The main simulation type. You give this a scene to update and render.
 */
class Simulation {
    /**
     * @param {Number] framerate How often to update and render in frames per second.
     */
    constructor(framerate = 60) {
        this.framerate = 1000 / framerate;
        this.starttime = 0;
        this.prevtime = 0;
        this.curtime = 0;
        this.scene = null;
        this._started = false;
        this._finish = false;
        this.counter = document.getElementById("counter");
    }
    
    /**
     * Start the simulation with a given scene.
     * @param {Scene} scene The scene to simulate.
     */
    start(scene) {
        if (!this._started) {
            this._started = true;
            this.scene = scene;
            this.starttime = performance.now();
            this.prevtime = this.starttime;
            this.curtime = this.prevtime;
            this.frames = 0;
            this._loop(this.curtime);
        }
    }
    
    /**
     * @param {Number} t Timestamp from requestAnimationFrame.
     */
    _loop(t) {
        if (this._finish) {
            return;
        }
        
        requestAnimationFrame((t) => this._loop(t));
        
        this.curtime = t;
        const diff = this.curtime - this.prevtime;
        const dt = diff * 0.001;
        
        if (diff >= this.framerate) {
            this.prevtime = this.curtime - (diff % this.framerate);
            
            this.scene.update(dt);
            this.frames += 1;
            this.counter.textContent = `${Math.round(1000 / ((this.curtime - this.starttime) / this.frames) * 100 * 0.01).toFixed(2)}`
        }
        
        this.scene.render(dt);
    }
}
