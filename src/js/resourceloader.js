"use strict";

/**
 * Resource type enum.
 * @readonly
 * @enum
 */
const RESOURCE = {
    TEXTURE: 0,
    SHADER: 1,
    AUDIO: 2
};

class ResourceLoader {
    constructor() {
        this._textures = {};
        this._shaders = {};
        this._audios = {};
    }
    
    /**
     * @overload
     * @param {Number} type Resource type.
     * @param {Array<[String, String, Object?]>} texture_reqinfo Texture request info. Key, path and options.
     *//**
     * @overload
     * @param {Number} type Resource type.
     * @param {Array<[String, String, String, Object?]>} shader_reqinfo Shader request info. Key, vertex and fragment shader paths and uniforms.
     * *//**
     * @overload
     * @param {Number} type Resource type.
     * @param {Array<[String, String, Boolean]>} audio_reqinfo Audio request info. Key, path and looped.
     */
    async load(type, reqinfo = []) {
        switch (type) {
            case RESOURCE.TEXTURE: { await this._loadTextures(reqinfo); } break;
            case RESOURCE.SHADER: { await this._loadShaders(reqinfo); } break;
            case RESOURCE.AUDIO: { await this._loadAudios(reqinfo); } break;
        }
    }
    
    _loadTextures(reqinfo) {
        const sync = CounterSync(reqinfo.length);
        
        // opts is optional. Texture defends against incorrect/missing input.
        for (const [key, path, opts] of reqinfo) {
            fetch(path).then(r => r.blob()).then(r => {
                const img = new Image();
                
                img.onload = () => {
                    if (this._textures.hasOwnProperty(key)) {
                    this._textures[key].unload();
                    }
                    
                    this._textures[key] = new Texture(img, opts);
                    sync.test();
                };
                
                img.src = URL.createObjectURL(r);
            });
        }
        
        return sync.sync;
    }
    
    _loadShaders(reqinfo) {
        const sync = CounterSync(reqinfo.length);
        
        // uniforms is optional. Shader defends against incorrect/missing input.
        for (const [key, vspath, fspath, uniforms] of reqinfo) {
            (async _ => {
                const innersync = CounterSync(2);
                let vtext;
                let ftext;
                
                fetch(vspath).then(r => r.text()).then(r => {
                    vtext = r;
                    innersync.test();
                });
                
                fetch(fspath).then(r => r.text()).then(r => {
                    ftext = r;
                    innersync.test();
                });
                
                await innersync.sync;
                
                if (this._shaders.hasOwnProperty(key)) {
                    this._shaders[key].unload();
                }
                
                this._shaders[key] = new Shader(vtext, ftext, uniforms);
                sync.test();
            })();
        }
        
        return sync.sync;
    }
    
    _loadAudios(reqinfo) {
        const sync = CounterSync(reqinfo.length);
        
        for (const [key, path, looped] of reqinfo) {
            fetch(path).then(r => r.arrayBuffer()).then(r => {
                if (this._audios.hasOwnProperty(key)) {
                    this._audios[key].unload();
                }
                
                if (looped) {
                    this._audios[key] = new LoopedAudioSource(r);
                }
                else {
                    this._audios[key] = new AudioSource(r);
                }
                
                sync.test();
            });
        }
        
        return sync.sync;
    }
    
    /**
     * Get a resource by key. Returns null if the resource is non-existant.
     * @param {Number} type One of RESOURCE.
     * @param {String} key Name of loaded resource.
     * @returns {Texture | Shader | Audio_ | null} A resource or null.
     */
    resource(type, key) {
        switch (type) {
            case RESOURCE.TEXTURE: return this._textures[key] || null;
            case RESOURCE.SHADER: return this._shaders[key] || null;
            case RESOURCE.AUDIO: return this._audios[key] || null;
        }
    }
}

const rl = new ResourceLoader();
