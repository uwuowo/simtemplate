"use strict";

/**
 * The base scene type.
 */
class Scene {
    update(dt) {}
    render(dt) {}
}
