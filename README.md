Template project for simulations, mostly particle stuff.

Most rendering is WebGL2, text isn't because 3D graphics APIs make performant,
(memory) efficient Unicode text rendering pain. I just use styled DOM nodes
for text.

Very annoyingly you need to run an HTTP server for this. You can't load local
files even if you open from a local file. Python has a built-in HTTP server
that this template uses by default. There is a helper script for Linux and
Windows.
